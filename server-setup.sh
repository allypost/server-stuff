#!/bin/bash

#--###################################################--#
# VARIABLE DEFINITIONS
_IDENTIFIER='# -- -.-- / .--- --- -... / .... . .-. . / .. ... / -.. --- -. . .-.-.- / .--. .-.. . .- ... . / --. --- / .- .-- .- -.--';
_BASH_HOME="$HOME/.bashrc";
#--###################################################--#


#--###################################################--#
# BASIC UPDATE, UNINSTALL BLOAT AND INSTALL USEFUL PROGRAMS
sudo apt-get remove apache2 -y;
sudo apt-get update;
sudo apt-get upgrade -y;
sudo apt-get dist-upgrade -y;
sudo apt-get autoremove -y;
sudo apt-get autoclean -y;
sudo apt-get install nano bash-completion -y;
#--###################################################--#


#--###################################################--#
# APPEND USEFUL STUFF TO THE LOCAL ~/.bashrc
_APPENDIX="$(cat <<-'EOF'
#--###################################################--#
# Additional code appended by automated script
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

random-string()
{
    cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-32} | head -n 1
}
#--###################################################--#

EOF
)"
_APPENDIX="$_APPENDIX$_IDENTIFIER";

if [ ! -f "$_BASH_HOME" ]; then
	sudo touch "$_BASH_HOME";
fi

if grep -rq "$_IDENTIFIER" "$_BASH_HOME"; then
    echo "DON'T APPEND";
else
    echo "APPEND";
    echo "$_APPENDIX" >> "$_BASH_HOME";
    #sed "/export\ PATH/i $_APPENDIX" "$_BASH_HOME";
fi
#--###################################################--#


#--###################################################--#
# SET LOCAL TIME TO UTC
sudo ln -fs /usr/share/zoneinfo/UTC /etc/localtime
#--###################################################--#


#--###################################################--#
# DELETE SELF AND REBOOT
sudo shutdown -r now;
sudo rm -- "$0";
#--###################################################--#